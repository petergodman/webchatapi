application = {
    debugmode : true,
    initialised:false,
    history:[],

    request: function (requestType, request, params, eventName) {
        var xmlHttpReq = false;

        if (window.XMLHttpRequest) {
            xmlHttpReq = new XMLHttpRequest();
        } else if (window.ActiveXObject) {
            try {
                xmlHttpReq = new ActiveXObject('Msxml2.XMLHTTP');
            } catch (e) {
                try {
                    xmlHttpReq = new ActiveXObject('Microsoft.XMLHTTP');
                } catch (e) {
                    alert('Your browser does not support AJAX!');
                    return false;
                }
            }
        }

        url = '/'+request;
        xmlHttpReq.owner = this;

        xmlHttpReq.open(requestType, url, true);
        xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=utf-8');

        if (this.authorisation) {
            xmlHttpReq.setRequestHeader('Authorization', 'Bearer '+this.authorisation);
        }

        xmlHttpReq.processResponse = function (responseText) {
            responseText = trim(responseText);
            this.hasErrors = false;


            this.result = JSON.parse(responseText);

            if (isset(this.result.errors)) {
                this.hasErrors = true;
                var errorCallback = '';
                this.result.errorMessage = '';
                code = '';

                if (isset(this.result.errors.error)) {
                    this.result.errorMessage = this.result.errors.error;
                } else {
                    if (isset(this.result.error)) {
                        this.result.errorMessage = this.result.error;
                    } else {
                        for (i = 0; i < this.result.errors.length; i++) {
                            var error = this.result.errors[i];

                            var messageTmp;
                            messageTmp = error;

                            if ((!isset(messageTmp)) || (messageTmp == null) || (messageTmp == 'null')) {
                                messageTmp = '<li>' + error + '</li>';
                            }

                            this.result.errorMessage = this.result.errorMessage + messageTmp + '\n';
                        }
                    }
                }

                this.result.hasErrors = this.hasErrors;
                this.result.errorCount = this.result.errorCount = this.result.errors.length;
            }


            application.response = this.result;
        };

        xmlHttpReq.onComplete = eventName;

        xmlHttpReq.onreadystatechange = function () {
            var responseText = '';
            if (this.readyState == 4) {
                if (this.status == 200) {
                    responseText = this.responseText;

                    authorisation = this.getResponseHeader('Authorization');

                    if (authorisation) {
                        application.authorisation = authorisation;
                    }

                    if (!empty(responseText)) {
                        this.processResponse(responseText);

                        if (!this.hasErrors) {
                            if (!empty(this.onComplete)) {
                                const loginEvent = new CustomEvent(this.onComplete, {
                                    detail: this.result,
                                    bubbles: true,
                                    cancelable: true,
                                    composed: false,
                                  });
                
                                  document.dispatchEvent(loginEvent);               
                            }
                        }
                    }
                } else {
                    application.log('Failed status: ' + this.status);
                    var errortext = application.translation.get('therehasbeenaproblemcommunicatingwiththeserverpleasecontactsupportiftheproblempersists', 'There has been a problem communicating with the server. Please contact support if the problem persists');
                    console.log(errortext);
                }
            }
        };

        if (!isNull(params)) {
            var paramsOut = JSON.stringify(params, function (k, v) {
                if (v instanceof Array) {
                    var o = {};
                    for (var ind in v) {
                        if (v.hasOwnProperty(ind)) {
                            o[ind] = v[ind];
                        }
                    }

                    return o;
                }

                return v;
            });
        }
        else {
            paramsOut = [];

        }
       
        xmlHttpReq.send(paramsOut);

    },

    log: function (message) {
        if (this.debugmode) {
            console.log(message);
        }
    },

    init : function () {
        document.addEventListener("updateConversation", (event) => {
            conversationId = event.detail.conversationId ;
            messages = event.detail.messages;

            messages.forEach(v => {
                application.history[conversationId] = v.id;
            });
        });            

        document.addEventListener("updateReply", (event) => {
            application.conversation(event.detail.conversationId);
        });            
    },
    
    startConversation : function () {
        userName = document.getElementById('username');

        if (userName != undefined) {
            params = {};
            params.name = userName.value;

            if (params.userId != '') {
                document.addEventListener("newconversation", (event) => {
                    application.conversationId = event.detail.conversationId;
                    
                });            
    
                this.request('POST', 'conversation/start', params, 'newconversation') ;
            }
            else {
                console.log('User Name required')
            }
        }
        else {
            console.log('User Name not found')
        }
        
    },

    login : function () {
        if (!this.initialised) {
            this.init();
        }

        this.authorisation = false ;

        userId = document.getElementById('userid');
        password = document.getElementById('password');

        if ((userId != undefined) && (password != undefined)) {
            params = {};
            params.userId = userId.value;
            params.password = password.value;

            if ((params.userId != '') && (params.password !='')) {
                this.request('POST', 'admin/login', params, 'login') ;
            }
            else {
                console.log('User id and password required')
            }
        }
        else {
            console.log('User id or password not found')
        }
    },

    conversations : function () {
        application.request('GET', 'admin/conversations', null, 'refreshConversationsList') ;
    },

    conversation : function (conversationId) {
        lastMessageId = application.history[conversationId];

        url = 'admin/conversation/'+conversationId;

        if (lastMessageId) {
            url = url+'/'+lastMessageId
        }

        application.request('GET', url, null, 'updateConversation') ;
    },

    adminSendMessage : function (conversationId, message) {
        application.request('POST', 'admin/conversation/'+conversationId, {message:message}, 'updateReply') ;
    },

    sendMessage : function (message) {
        application.request('POST', 'conversation/message'+application.conversationId, {message:message}, 'updateReply') ;
    },

    updateChat : function (message) {
        application.request('POST', 'conversation/'+application.conversationId+'/update', {message:message}, 'updateReply') ;
    }

}

function isset(a) {
    return !isNull(a) && a !=undefined ;
}

function isNull(a) {
    var result = true ;
  
    if ((a != null) && (a != 'null') && (a != 'undefined') && (a != undefined)) {
      result = false ;
    }
    else if ((typeof a != 'object') && (typeof a != 'string')) {
      result = false ;
    }
  
    return result;
  }

  function empty (mixed_var) {
    // Checks if the argument variable is empty
    // undefined, null, false, number 0, empty string,
    // string "0", objects without properties and empty arrays
    // are considered empty
    //
    // From: http://phpjs.org/functions
    // +   original by: Philippe Baumann
    // +      input by: Onno Marsman
    // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
    // +      input by: LH
    // +   improved by: Onno Marsman
    // +   improved by: Francesco
    // +   improved by: Marc Jansen
    // +      input by: Stoyan Kyosev (http://www.svest.org/)
    // +   improved by: Rafal Kukawski
    // *     example 1: empty(null);
    // *     returns 1: true
    // *     example 2: empty(undefined);
    // *     returns 2: true
    // *     example 3: empty([]);
    // *     returns 3: true
    // *     example 4: empty({});
    // *     returns 4: true
    // *     example 5: empty({'aFunc' : function () { alert('humpty'); } });
    // *     returns 5: false
    var undef, key, i, len;
    var emptyValues = [undef, null, false, 0, "", "0"];
  
    for (i = 0, len = emptyValues.length; i < len; i++) {
      if (mixed_var === emptyValues[i]) {
        return true;
      }
    }
  
    if (typeof mixed_var === "object") {
      for (key in mixed_var) {
        // TODO: should we check for own properties only?
        //if (mixed_var.hasOwnProperty(key)) {
        return false;
        //}
      }
      return true;
    }
  
    return false;
  }

  /**
 * 
 * @param {type} str
 * @param {type} chars
 * @returns {unresolved}
 */
function trim(str, chars) {
    return ltrim(rtrim(str, chars), chars);
  }
  /**
   * 
   * @param {type} str
   * @param {type} chars
   * @returns {unresolved}
   */
  function ltrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
  }
  /**
   * 
   * @param {type} str
   * @param {type} chars
   * @returns {unresolved}
   */
  function rtrim(str, chars) {
    chars = chars || "\\s";
    return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
  }
  