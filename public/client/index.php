<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="index, follow, all" />

        <script src="webchat.js"></script>

        <title>Web Chat Admin Client</title>

<style>

.tabs {
  display: flex;
  flex-wrap: wrap;
}

.tabs label {
  order: 1;
  display: block;
  padding: 1rem 2rem;
  margin-right: 0.2rem;
  cursor: pointer;
  background:  #3CC0F8;
  font-weight: bold;
  transition: background ease 0.2s;
}

.tabs .tab  {
  order: 99;
  flex-grow: 1;
  width: 100%;
  display: none;
  padding: 1rem;
  background: #fff;
}

.tabs .reply {
  order: 99;
  flex-grow: 1;
  width: 100%;
  display: none;
  padding: 1rem;
  background: #fff;
}

.tabs input[type="radio"] {
  display: none;
}

.tabs input[type="radio"]:checked + label {
  background: #fff;
}

.tabs input[type="radio"]:checked + label + .tab {
  display: block;
}

.tabs input[type="radio"]:checked + label +  .tab + .reply {
  display: block;
}

@media (max-width: 45em) {
  .tabs .tab,
  .tabs .reply,
  .tabs label {
    order: initial;
  }

  .tabs .reply {
    width: 100%;
    margin-right: 0;
    margin-top: 0.2rem;
  }

  .tabs label {
    width: 100%;
    margin-right: 0;
    margin-top: 0.2rem;
  }
}

body {
  background: #333;
  min-height: 100vh;
  box-sizing: border-box;
  padding-top: 10vh;
  font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif;
  font-weight: 300;
  line-height: 1.5;
  max-width: 60rem;
  margin: 0 auto;
  font-size: 112%;
}
</style>        
    </head>

    <body class="" >
        <form id="loginform" onSubmit="application.login(); return false;">
            <p>
            <label for="userid">User Id</label><input type="text" id="userid" name="userid" />
            </p>
            <p>
                <label for="password">Password</label><input type="password" id="password" name="password" />
            </p>
            <p>
                <input id="login" name="login"  type="submit" />
            </p>
        </form>

        <div class="tabs" id="tabs">
        </div>

        <script type="text/javascript">
            function sendMessage(button) {
                const p = button.parentNode;
                
                const conversationId = button.dataset.conversationid;

                textAreas = p.getElementsByTagName('textarea');

                message = textAreas[0].value;
                application.adminSendMessage(conversationId, message);

//                body = document.getElementById('body-'+conversationId);
//                body.innerHTML = body.innerHTML +'<p><b>You</b>:'+message+'</p>';
            }

            document.addEventListener("login", (event) => {

                if (application.authorisation) {
                    loginForm = document.getElementById('loginform') ;

                    if (loginForm) {
                        loginForm.style.display='none';
                    }
                
                    application.conversations();
                }
                else {
                    alert('Login failed');
                }
            });

            document.addEventListener("refreshConversationsList", (event) => {
                tabs = document.getElementById('tabs');

                event.detail.forEach(v => {
                    newTab = '<input type="radio" name="tabs" id="tab-'+v.id+'" checked="checked" onCLick="application.conversation(\''+v.id+'\')">';
                    newTab = newTab + '<label for="tab-'+v.id+'">'+v.name+'</label>';
                    newTab = newTab + '<div class="tab" id="body-'+v.id+'">'+v.id;
                    newTab = newTab + '</div>';
                    newTab = newTab + '<div class="reply">Reply:<textarea></textarea><button data-conversationid="'+v.id+'" onclick="sendMessage(this)">send</button></div>';

                    tabs.innerHTML = tabs.innerHTML + newTab;

                });
            });            

            document.addEventListener("updateConversation", (event) => {
                conversationId = event.detail.conversationId ;
                messages = event.detail.messages;

                body = document.getElementById('body-'+conversationId);

                messages.forEach(v => {
                    body.innerHTML = body.innerHTML + '<p><b>'+v.from+'</b>:'+v.message+'</p>';
                });

                //console.log(event.detail);                
            });            

        </script>
    </body>
